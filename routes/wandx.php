<?php

Route::group(['namespace'=>'Auth'],function(){
    Route::get('/',['middleware'=>'guest:admin','uses'=>'LoginCont@index'])->name('adm.login');
    Route::post('login','LoginCont@doLogin')->name('adm.login.post');
    Route::get('roles',['middleware'=>'auth:admin','uses'=>'LoginCont@roles'])->name('adm.select_role');
    Route::post('roles',['middleware'=>'auth:admin','uses'=>'LoginCont@rolesPost'])->name('adm.select_role.post');
});

Route::group(['middleware'=>['auth:admin','hasmanyrole']],function(){
    // Logout
    Route::get('logout','Auth\LoginCont@logout');
    // Dashboard
    Route::group(['namespace'=>'Dashboard','prefix'=>'dashboard','middleware'=>'access:dashboard'],function(){
        Route::get('/','DashboardCont@index')->name('adm.dashboard');
    });

    // Misc
    Route::group(['prefix'=>'misc'],function(){
        Route::get('city',function(){
            return (new \App\Libs\RajaOngkir())->city();
        });

        Route::get('cost',function(){
            $courier = Request::input('courier');
            $origin = Request::input('origin');
            $destination = Request::input('destination');
            $weight = Request::input('weight');

            return (new \App\Libs\RajaOngkir())->cost($courier,$origin,$destination,$weight);
        });
    });

    // User Manager
    Route::group(['namespace'=>'Usermanager','prefix'=>'usermanager','middleware'=>'access:usermanager'],function(){
        Route::get('lists','UserCont@lists')->name('adm.usermanager.lists');
        Route::post('store','UserCont@store')->name('adm.usermanager.store');
        Route::get('destroy/{id}','UserCont@destroy');
        Route::post('update/{id}','UserCont@update');
        Route::get('userdata/{id}',function($id){
            return (new \App\Models\Admin())->newQuery()->where('id',$id)->with('roles')->first();
        });

        Route::group(['prefix'=>'roles'],function(){
            Route::get('/','RoleCont@index')->name('adm.roles');
            Route::get('menu_ids/{role_id}','RoleCont@menuIds');
            Route::post('update_menu','RoleCont@updateMenu')->name('adm.update_menu');
            Route::get('remove_access/{menu_id}/{role_id}','RoleCont@removeAccess');
        });
    });

    // Product
    Route::group(['prefix' => 'product','namespace'=>'Product','as'=>'product.'],function(){
        Route::get('/','ProductCont@index')->name('index');
        Route::get('data','ProductCont@data')->name('data');
        Route::get('detail/{id?}','ProductCont@detail')->name('detail');
        Route::get('suspend/{id}','ProductCont@suspend')->name('suspend');
        Route::get('restore/{id}','ProductCont@restore')->name('restore');
        Route::get('destroy/{id}','ProductCont@destroy')->name('destroy');
        Route::get('set-commission/{id}/{com}','ProductCont@set_commission')->name('set-commission');
    });

    // Member
    Route::group(['prefix'=> 'member','namespace'=>'Member','as'=>'member.'],function(){
        // User
        Route::group(['prefix'=>'user','as'=>'user.'],function(){
            Route::get('/','UserCont@index')->name('index');
            Route::get('data','UserCont@data')->name('data');
        });

        // Vendor
        Route::group(['prefix'=>'vendor','as'=>'vendor.'],function(){
            Route::get('/','VendorCont@index')->name('index');
            Route::get('data','VendorCont@data')->name('data');
        });

        // Vendor request
        Route::group(['prefix'=>'vendor-request','as'=>'vr.'],function(){
            Route::get('/','VendorRequestCont@index')->name('index');
            Route::get('data','VendorRequestCont@data')->name('data');
            Route::get('accept/{id}','VendorRequestCont@accept_request')->name('accept');
            Route::get('reject/{id}','VendorRequestCont@reject_request')->name('reject');
        });

        Route::get('suspend/{id}','UserCont@suspend')->name('suspend');
        Route::get('restore/{id}','UserCont@restore')->name('restore');
        Route::get('destroy/{id}','UserCont@destroy')->name('destroy');
    });

    // Pos
    Route::group(['prefix'=>'pos','namespace'=>'PointOfSale','as'=>'pos.'],function(){
        Route::get('/','PosCont@index')->name('index');
        Route::get('product_data','PosCont@product_data')->name('product_data');
        Route::get('fetch_carts','PosCont@fetch_product_by_carts')->name('fetch_carts');
        Route::get('add_cart/{id}','PosCont@add_cart')->name('add_cart');
        Route::get('del_cart/{id}','PosCont@del_cart')->name('del_cart');
    });
});