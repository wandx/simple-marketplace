<?php

namespace App\Http\Controllers\Wandx\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardCont extends Controller
{
    public function index(){
        return view('wandx.dashboard.index');
    }
}
