<?php

namespace App\Http\Controllers\Wandx\Usermanager;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class UserCont extends Controller
{
    public function lists(Request $request,Admin $admin){
        $data = [
            'lists' => $this->userQuery($request,$admin)
        ];

        return view('wandx.usermanager.lists',$data);
    }

    public function store(Request $request,Admin $admin){
        $data = [
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'api_token' => str_random(60)
        ];

        if($request->hasFile('avatar')){
            $name = Str::slug($request->input('name')).'-'.rand(1000,9999).'.jpg';
            $path = 'uploads/avatar/'.$name;

            Image::make($request->file('avatar'))->fit(200,200)->save($path);
            $data['avatar'] = $path;
        }

        $store = $admin->create($data);

        if($request->has('roles')){
            $store->roles()->attach($request->input('roles'));
        }

        return redirect()->back()->with(['success'=>$store->name.' berhasil ditambahkan']);
    }

    public function update($id,Request $request,Admin $admin){
        $data = [
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'api_token' => str_random(60)
        ];

        if($request->has('password')){
            $data['password'] = $request->has('password') ?bcrypt($request->input('password')):'';
        }

        if($request->hasFile('avatar')){
            $this->deleteAvatar($id,$admin);
            $name = Str::slug($request->input('name')).'-'.rand(1000,9999).'.jpg';
            $path = 'uploads/avatar/'.$name;

            Image::make($request->file('avatar'))->fit(200,200)->save($path);
            $data['avatar'] = $path;
        }

        $store = $admin->find($id)->update($data);
        $store = $admin->find($id);

        if($request->has('roles')){
            $store->roles()->sync($request->input('roles'));
        }

        return redirect()->back()->with(['success'=>$store->name.' berhasil diubah']);
    }

    public function destroy($id,Admin $admin){
        $admin->newQuery()->find($id)->delete();
    }

    private function userQuery(Request $request,Admin $admin){
        $admin = $admin->newQuery();
        $admin->with('roles');

        return $admin->paginate(10);
    }

    private function deleteAvatar($id,Admin $admin){
        $avatar = $admin->newQuery()->find($id)->avatar;

        try{
            if(file_exists($avatar) && !is_dir($avatar)){
                unlink($avatar);
            }
        }catch (\Exception $e){}
    }
}
