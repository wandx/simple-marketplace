<?php

namespace App\Http\Controllers\Wandx\Member;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class VendorRequestCont extends Controller
{
    public function index(){
        return view('wandx.member.vendor_request');
    }

    public function data(User $user){
        $where  = function($query){
            $query->where('is_vendor',0)->where('request_vendor',1)->where('status','active');
            $query;
            $query;
        };

        return DataTables::of($user->newQuery()->where($where))
            ->addColumn('actions',function($data){
                $act = '<button type="button" data-id="'.$data->id.'"  data-name="'.$data->name.'" class="btn btn-default btn-xs accept-btn"><i class="fa fa-thumbs-up"></i></a>';
                $act .= '<button type="button" data-toggle="modal" data-target="#detail-product" data-id="'.$data->id.'" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
                $act .= '<button type="button" data-id="'.$data->id.'"  data-name="'.$data->name.'" class="btn btn-default btn-xs reject-btn"><i class="fa fa-thumbs-down"></i></a>';
//                if($data->status == "suspend"){
//                    $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs restore-btn"><i class="fa fa-refresh"></i></button>';
//                }else if($data->status == "active"){
//                    $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs suspend-btn"><i class="fa fa-warning"></i></button>';
//                }
//                $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs destroy-btn"><i class="fa fa-trash"></i></button>';

                return $act;
            })
            ->addColumn('merchant',function($data){
                return $data->merchant->name ?? "-";
            })
            ->rawColumns(['actions','merchant'])
            ->make(true);
    }

    public function accept_request($id,User $user){
        $user->newQuery()->find($id)->update([
            'is_vendor' => 1,
            'request_accepted_date' => Carbon::now()
        ]);
    }

    public function reject_request($id,User $user){
        $user->newQuery()->find($id)->update([
            'request_vendor' => 0
        ]);
    }
}
