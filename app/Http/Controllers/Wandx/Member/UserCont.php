<?php

namespace App\Http\Controllers\Wandx\Member;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class UserCont extends Controller
{
    public function index(){
        return view('wandx.member.user');
    }

    public function data(User $user){
        return DataTables::of($user->newQuery())
            ->addColumn('actions',function($data){
                $act = "";
//                $act = '<a href="#" data-toggle="modal" data-target="#detail-product" data-id="'.$data->id.'" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
                if($data->status == "suspend"){
                    $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs restore-btn"><i class="fa fa-refresh"></i></button>';
                }else if($data->status == "active"){
                    $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs suspend-btn"><i class="fa fa-warning"></i></button>';
                }
                $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs destroy-btn"><i class="fa fa-trash"></i></button>';

                return $act;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function suspend($user_id,User $user){
        $user->newQuery()->find($user_id)->update(['status'=>'suspend']);
    }

    public function restore($user_id,User $user){
        $user->newQuery()->find($user_id)->update(['status'=>'active']);
    }

    public function destroy($user_id,User $user){
        $user->newQuery()->find($user_id)->delete();
    }
}
