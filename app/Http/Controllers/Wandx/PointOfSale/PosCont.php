<?php

namespace App\Http\Controllers\Wandx\PointOfSale;

use App\Libs\RajaOngkir;
use App\Models\Cities;
use App\Models\Product;
use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class PosCont extends Controller
{
    public function index(){
        Session::forget('carts');
        return view('wandx.pos.index');
    }

    public function product_data(Product $product){
        $where = function($query){
            $query->where('status','active');
            $query->where('quantity','>',0);
            $query->whereHas('user',function($model){
                $model->whereHas('merchant',function($m){
                    $m->where('status','active');
                })->where('status','active');
            });

            if(Session::has('carts')){
                $query->whereNotIn('id',session('carts'));
            }
        };
        return DataTables::of($product->newQuery()->where($where))
            ->addColumn('actions',function($data){
                $act = '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs add-cart-btn"><i class="fa fa-plus"></i></button>';

                return $act;
            })
            ->addColumn('merchant',function($data){
                return $data->user->merchant->name ?? '-';
            })
            ->addColumn('cover_img',function($data){
                return "<img src='".asset($data->cover)."' class='img-circle img50' />" ?? '-';
            })
            ->rawColumns(['actions','cover_img'])
            ->make(true);
    }

    public function add_cart($id){
        if(Session::has('carts')){
            Session::push('carts',$id);
        }else{
            Session::put('carts',[$id]);
        }
        return session('carts');
    }

    public function del_cart($id){
        if(Session::has('carts')){
            $carts = Session::get('carts');
            if(!is_array($carts)){
                $carts = $carts->toArray();
            }
            if(($key = array_search($id,$carts)) !== false){
                unset($carts[$key]);
            }
            session()->put('carts',$carts);
        }
    }

    public function fetch_product_by_carts(Product $product){
        if(Session::has('carts')){
            $carts = session('carts');
        }else{
            $carts = [];
        }
        $data = [
            'products' => $product->whereIn('id',$carts)->get(),
            'cities' => (new Cities())->select('id',DB::raw('CONCAT(name," ( ",type," )") as cname'))->pluck('cname','id')
        ];


        return view('wandx.pos.carts',$data);
    }

    public function get_city($id=null){
        return (new RajaOngkir())->get_city();

    }
}
