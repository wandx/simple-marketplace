<?php

namespace App\Http\Controllers\Wandx\Product;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class ProductCont extends Controller
{
    public function index(){
        return view('wandx.product.index');
    }

    public function data(Product $product){
        return DataTables::of($product->newQuery())
                        ->addColumn('actions',function($data){
                            $act = '<a href="#" data-toggle="modal" data-target="#detail-product" data-id="'.$data->id.'" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>';
                            if($data->status == "suspend"){
                                $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs restore-btn"><i class="fa fa-refresh"></i></button>';
                            }else if($data->status == "active"){
                                $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs suspend-btn"><i class="fa fa-warning"></i></button>';
                            }
                            $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" data-toggle="modal" data-target="#set-commission" class="btn btn-default btn-xs commission-btn"><i class="fa fa-money"></i></button>';
                            $act .= '<button type="button" data-id="'.$data->id.'" data-name="'.$data->name.'" class="btn btn-default btn-xs destroy-btn"><i class="fa fa-trash"></i></button>';
        
                            return $act;
                        })
                        ->addColumn('merchant',function($data){
                            return $data->user->merchant->name ?? '-';
                        })
                        ->addColumn('cover_img',function($data){
                            return "<img src='".asset($data->cover)."' class='img-circle img50' />" ?? '-';
                        })
                        ->rawColumns(['actions','cover_img'])
                        ->make(true);
    }

    public function detail($product_id,Product $product){
        $data = [
            'product' => $product->newQuery()->find($product_id)
        ];
        return view('wandx.product.detail',$data);
    }

    public function suspend($product_id,Product $product){
        $product->newQuery()->find($product_id)->update(['status'=>'suspend']);
    }

    public function restore($product_id,Product $product){
        $product->newQuery()->find($product_id)->update(['status'=>'active']);
    }

    public function destroy($product_id,Product $product){
        $product->newQuery()->find($product_id)->delete();
    }

    public function set_commission($id,$com,Product $product){
        $product->newQuery()->find($id)->update([
            'commission' => $com
        ]);
    }
}
