<?php

namespace App\Http\Middleware\Wandx;

use Closure;

class HasManyRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $cek = auth('admin')->user()->roles->count();

        if(!session('role_selected')) {
            // if user has more than one role, redirect to role selection
            if($cek > 0){
                return redirect()->route('adm.select_role');
            }else{
                session(['role_selected'=>true,'role_id'=>auth()->user()->roles->first()]);
            }
        }
        return $next($request);
    }
}
