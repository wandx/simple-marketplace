<?php namespace App\Libs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class RajaOngkir{
    private $key = "bbe6b68ddbb8e32290f1d98d2f5852c3";
    private $base_url = "https://api.rajaongkir.com/starter/";


    public function city(){
        $client = new Client();
        try{
            $req = $client->get($this->base_url.'city',[
                "headers" => [
                    "key" => $this->key,
                    "Content-Type" => "application/x-www-form-urlencoded"
                ]
            ]);
            return response()->json($req);
        }catch(ClientException $e){
            return $e->getMessage();
        }
    }

    public function cost($courier,$origin,$destination,$weight){
        $client = new Client();
        try{
            $req = $client->post($this->base_url.'cost',[
                "headers" => [
                    "key" => $this->key,
                    "Content-Type" => "application/x-www-form-urlencoded"
                ],
                "form_params" => [
                    "origin" => $origin,
                    "destination" => $destination,
                    "weight" => $weight,
                    "courier" => $courier
                ]
            ]);
            return $req;
        }catch(ClientException $e){
            return $e->getMessage();
        }
    }
}