<?php

// Build menu
function menuData(){
    $menu = new \App\Models\Menu();
    $menu = $menu->newQuery()->whereHas('roles',function($role){
        $role->where('id',session('role_id'));
    })->with('sub_menus.sub_sub_menus')->get();

    return $menu;
}

// get all menu
function allMenuData(){
    $menu = new \App\Models\Menu();
    $menu = $menu->newQuery()->with('sub_menus.sub_sub_menus')->get();

    return $menu;
}

// Get role by id
function roleName($id){
    $role = new \App\Models\Role();
    $role = $role->newQuery()->find($id);

    return $role == null ? "-" : $role->name;
}

// Get last activity (admin)
function lastActivity(){
    if(auth('admin')->check()){
        $ac = new \App\Models\AdminActivity();
        $ac = $ac->newQuery()->whereHas('admin',function($admin){
            $admin->where('id',auth('admin')->user()->id);
        })->orderBy('time','desc')->first();

        return $ac == null ? "no activity yet":$ac->time->format('d M Y, H:i:s');
    }
    return "no activity";
}

function roleData(){
    $role = new \App\Models\Role();
    return $role->newQuery()->pluck('name','id');
}

function backend_url($path){
    return url("cahbackend/".$path);
}

function nf($num){
    return number_format($num,0,',','.');
}

function section($name){
    $sec = (new \App\Models\Section())->newQuery()->whereName($name)->first();
    return $sec->value ?? "";
}