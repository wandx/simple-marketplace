<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $guarded = ['id'];

    public function merchant_accounts(){
        return $this->hasMany(MerchantAccount::class);
    }
}
