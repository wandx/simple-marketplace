<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class AdminActivity extends Model
{
    use UuidForKey;
    public $incrementing = false;
    public $timestamps = false;
    protected $dates = ['time'];
    protected $guarded = ['id'];

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_id');
    }
}
