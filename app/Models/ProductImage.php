<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use UuidForKey;

    protected $guarded = ['id'];
    public $incrementing = false;

    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
