<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $guarded = ['id'];
    public $incrementing = false;
    public $timestamps = false;

    public function user(){
        return $this->belongsTo(User::class);
    }
}
