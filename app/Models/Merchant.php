<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    use UuidForKey;

    protected $guarded = ['id'];
    public $incrementing = false;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function merchant_accounts(){
        return $this->hasMany(MerchantAccount::class);
    }

    public function city(){
        return $this->belongsTo(Cities::class);
    }
}
