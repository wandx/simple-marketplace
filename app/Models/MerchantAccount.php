<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Database\Eloquent\Model;

class MerchantAccount extends Model
{
    use UuidForKey;

    protected $guarded = ['id'];
    public $incrementing = false;

    public function bank(){
        return $this->belongsTo(Bank::class);
    }
}
