<?php

namespace App\Models;

use App\Traits\UuidForKey;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,UuidForKey;

    protected $guarded = ['id'];
    public $incrementing = false;

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function addresses(){
        return $this->hasMany(Address::class);
    }

    public function merchant(){
        return $this->hasOne(Merchant::class);
    }
}
