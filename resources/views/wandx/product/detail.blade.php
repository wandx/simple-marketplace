<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a data-toggle="tab" href="#general-info">General Info</a></li>
            <li><a data-toggle="tab" href="#images">Images</a></li>
            {{--<li><a data-toggle="tab" href="#reviews">Reviews</a></li>--}}
        </ul>
        <br>
        <div class="tab-content">
            <div id="general-info" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Name</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ $product->name }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Status</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ strtoupper($product->status) }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Price</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ $product->price }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Rating</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ $product->rating }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Quantity</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ $product->quantity }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Category</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ $product->sub_category->category->name ?? "-" }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Sub Category</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ $product->sub_category->name ?? "-" }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Merchant</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ strtoupper($product->user->merchant->name) ?? "-" }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-4">Merchant Status</label>
                                <div class="col-sm-8">
                                    <span class="titikdua">{{ strtoupper($product->user->merchant->status) ?? "-" }}</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="row">
                            <div class="form-group">
                                <label for="" class="control-label col-sm-3">Description</label>
                                <div class="col-sm-9">
                                    <span class="titikdua">{{ $product->description ?? "-" }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="images" class="tab-pane fade">
                <div class="row">
                    @forelse($product->images as $image)
                        <div class="col-sm-4">
                            <img src="{{ asset($image->square) }}" alt="image" style="width: 100%;">
                        </div>

                    @empty
                        <div class="well text-center">No Image</div>
                    @endforelse
                </div>
            </div>
            {{--<div id="reviews" class="tab-pane fade">--}}
                {{--<h3>Iki review</h3>--}}
            {{--</div>--}}
        </div>
    </div>
</div>