@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Product</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Product
        <small>List of product</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>Cover</th>
                    <th>Name</th>
                    <th>Merchant</th>
                    <th>Status</th>
                    <th class="text-right">Commission</th>
                    <th class="text-right">Price</th>
                    <th class="text-right">Qty</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>
                        <select id="search-by-status" class="form-control">
                            <option value="">All</option>
                            <option value="active">Active</option>
                            <option value="pending">Pending</option>
                            <option value="suspend">Suspend</option>
                        </select>
                    </td>

                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop

@section('prestyle')
    <link rel="stylesheet" href="/wandx/css/dataTables.bootstrap.min.css">
@stop

@section('modals')
    <!-- Modal -->
    <div id="detail-product" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <div id="set-commission" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Set Commission</h4>
                </div>
                <form action="#" id="submit-commission">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="commission">Commission</label>
                            <input type="number" name="commission" id="commission" class="form-control">
                            <input type="hidden" name="id" id="id">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Set</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
@stop

@section('scripts')
    <script src="/wandx/js/jquery.dataTables.min.js"></script>
    <script src="/wandx/js/dataTables.bootstrap.min.js"></script>
    <script>
        var table;
        $(function(){
            table = $('#dtable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('product.data') }}",
                "columns": [
                    {"data":"cover_img","name":"cover_img","orderable":false,"searchable":false},
                    {"data":"name","name":"name"},
                    {"data": "merchant", "name": "merchant"},
                    {"data":"status","name":"status"},
                    {"data":"commission","name":"commission"},
                    {"data":"price","name":"price"},
                    {"data":"quantity","name":"quantity"},
                    {"data":"actions","name":"actions","orderable":false,"searchable":false}
                ],
                "columnDefs":[
                    {
                        "targets":4,
                        "className":"text-right"
                    },
                    {
                        "targets":5,
                        "className":"text-right"
                    },
                    {
                        "targets":6,
                        "className":"text-right"
                    },
                    {
                        "targets":7,
                        "className":"text-center"
                    }
                ]
            });

            // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'select#search-by-status', this.footer() ).on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );


        });

        $('#detail-product').on('show.bs.modal',function(e){
            var modal = $(this),
                trig = $(e.relatedTarget),
                id = trig.data('id');

            $.get('{{ backend_url('product/detail') }}/'+id,function(data){
                modal.find('.modal-body').html(data);
            })
        });

        $('#set-commission').on('show.bs.modal',function(e){
            var modal = $(this),
                trig = $(e.relatedTarget),
                id = trig.data('id'),
                name = trig.data('name');

            modal.find('.modal-title').html('Set Commission for '+name);
            modal.find('#id').val(id);

        });

        $('#submit-commission').submit(function(e){
            e.preventDefault();
            var id = $('#id').val();
            var com = $('#commission').val();
            var form = $(this);
            $.get('{{ backend_url("product/set-commission") }}/'+id+'/'+com,function(){
                table.ajax.reload();
                form.trigger('reset');
                $('#set-commission').modal('hide');
            });

        });

        $(document).on('click','.suspend-btn',function(){
            var btn = $(this);
            suspend(btn.data('id'),btn.data('name'));
        });

        $(document).on('click','.restore-btn',function(){
            var btn = $(this);
            restore(btn.data('id'),btn.data('name'));
        });

        $(document).on('click','.destroy-btn',function(){
            var btn = $(this);
            destroy(btn.data('id'),btn.data('name'));
        });





        function suspend(id,name){
            var c = confirm('Suspend '+name+' ?');
            if(c){
                $.get('{{ backend_url('product/suspend') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }

        function restore(id,name){
            var c = confirm('Restore '+name+' ?');
            if(c){
                $.get('{{ backend_url('product/restore') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }

        function destroy(id,name){
            var c = confirm('Destroy '+name+' ?');
            if(c){
                $.get('{{ backend_url('product/destroy') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }


    </script>
@stop

@section('styles')
    <style>
        .titikdua:before{
            content: ": ";
        }
    </style>
@stop