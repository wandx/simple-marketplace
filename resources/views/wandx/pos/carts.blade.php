<table class="table">
    @if($products->count() > 0)
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Cover</th>
            <th>Name</th>
            <th>Amount</th>
            <th class="text-right">Commission</th>
            <th class="text-right">Price</th>
        </tr>
        </thead>
    @endif
    <tbody>
    @forelse($products as $product)
        <tr>
            <td>
                <button class="btn btn-xs btn-danger del-cart-btn" type="button" data-id="{{ $product->id }}">
                    <i class="fa fa-trash"></i>
                </button>
            </td>
            <td><img src="{{ asset($product->cover) }}" class="img-circle img50" alt=""></td>
            <td>{{ $product->name }}</td>
            <td>
                <div class="input-group">
                    <input value="1" name="amount[]" type="number" class="form-control input-sm amount" max="{{ $product->quantity }}" data-price="{{ $product->price }}" data-commission="{{ $product->commission }}">
                    <span class="input-group-addon">
                        of {{ $product->quantity }}
                    </span>
                </div>

            </td>
            <td class="text-right">
                <span class="price-commission">{{ $product->commission }}</span>
                <input class="price-commission" type="hidden" name="price-commission[]" value="{{ $product->commission }}">
            </td>
            <td class="text-right">
                <span class="price-display">{{ $product->price }}</span>
                <input class="price" type="hidden" name="price[]" value="{{ $product->price }}">
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 1px solid black;">&nbsp;</td>
            <td style="border-bottom: 1px solid black;">
                <select name="" id="" class="form-control courier">
                    <option value="">Courier</option>
                    <option value="jne">JNE</option>
                    <option value="tiki">TIKI</option>
                    <option value="pos">POS</option>
                </select>
            <td style="border-bottom: 1px solid black;">
                {!! Form::select('origin',$cities,null,['class'=>'form-control','placeholder'=>'Origin']) !!}
            </td>

            <td style="border-bottom: 1px solid black;">
                {!! Form::select('destination',$cities,null,['class'=>'form-control','placeholder'=>'Destination']) !!}
            </td>

            <td style="border-bottom: 1px solid black;" class="text-right package">
                <select name="" id="" class="form-control">
                    <option value="">Package</option>
                </select>
            </td>

            <td class="text-right">
                0
            </td>
        </tr>
        @empty
        <div class="well text-center">
            No Data
        </div>


    @endforelse
    @if($products->count() > 0)
        <tr>
            <td style="border-top: 2px solid black;" class="text-right" colspan="6">
                <span class="price-total">
                    {{ $products->sum('price') + $products->sum('commission') }}
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="text-right" style="padding-right: 0;">
                <button class="btn btn-success">CHECKOUT</button>
            </td>
        </tr>
    @endif
    </tbody>
</table>