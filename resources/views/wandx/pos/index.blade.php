@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Point Of Sale</a></li>
    </ol>
@stop

@section('page-header')
    <h1>
        Point Of Sale
        <small>Point Of Sale</small>
    </h1>
@stop

@section('contents')
    <div class="panel panel-default" id="cart-container">
        <div class="panel-heading">
            <h3 class="panel-title">Shopping Cart</h3>
        </div>
        <div class="panel-body">
            <div class="well text-center">
                No Data
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Product Lists</h3>
        </div>
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>Cover</th>
                    <th>Name</th>
                    <th>Merchant</th>
                    <th class="text-right">Commission</th>
                    <th class="text-right">Price</th>
                    <th class="text-right">Qty</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('prestyle')
    <link rel="stylesheet" href="/wandx/css/dataTables.bootstrap.min.css">
@stop

@section('scripts')
    <script src="/wandx/js/jquery.dataTables.min.js"></script>
    <script src="/wandx/js/dataTables.bootstrap.min.js"></script>
    <script src="/wandx/js/ro.js"></script>
    <script>
        var table;
        $(function() {
            table = $('#dtable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('pos.product_data') }}",
                "columns": [
                    {"data": "cover_img", "name": "cover_img"},
                    {"data": "name", "name": "name"},
                    {"data": "merchant", "name": "merchant"},
                    {"data": "commission", "name": "commission"},
                    {"data": "price", "name": "price"},
                    {"data": "quantity", "name": "quantity"},
                    {"data":"actions","name":"actions","orderable":false,"searchable":false}
                ],
                "columnDefs": [
                    {
                        "targets": 3,
                        "className": "text-right"
                    },
                    {
                        "targets": 4,
                        "className": "text-right"
                    },
                    {
                        "targets": 5,
                        "className": "text-right"
                    },
                    {
                        "targets": 6,
                        "className": "text-center"
                    }
                ]
            });
        });

        $(document).on('click','.add-cart-btn',function(){
            // set data
            $.get('{{ backend_url("pos/add_cart") }}/'+$(this).data('id'),function(){
                setTimeout(redraw_cart(),1000);
            });

            // redraw

            // redraw_cart();
        });

        $(document).on('click','.del-cart-btn',function(){
            var id = $(this).data('id');
            $.get('{{ backend_url("pos/del_cart") }}/'+id,function(){
                setTimeout(redraw_cart(),1000);
            })
        });

        $(document).on('change keyup','.amount',function(e){
            var ts = $(this);
            var amount = isNaN(parseInt(ts.val())) ? 0 : parseInt(ts.val());
            var price = isNaN(parseInt(ts.data('price'))) ? 0 : parseInt(ts.data('price'));
            var commission = isNaN(parseInt(ts.data('commission'))) ? 0 : parseInt(ts.data('commission'));
            var total = amount*price;

            ts.parent().parent().parent().find('.price-display').html(total);
            ts.parent().parent().parent().find('.price').val(total);

            total_price(commission,amount);
        });

        $(document).on('click','.origin',function(e){
            get_city(e);
        });

        function total_price(com,amount) {
            var total = 0;

            var pr = $('.price-display');
            var commission = com*amount;



            if(pr.length > 0){
                $.each(pr,function(i,e){
                    total += parseInt($(e).text());
                })
            }

            $('.price-total').text(total+commission);
        }

        function redraw_cart(){
            $.get('{{ backend_url("pos/fetch_carts") }}',function(data){
                $('#cart-container').find('.panel-body').html(data);
            });
            table.ajax.reload();
        }

        function get_city(elem){
            $.getJSON('{{ backend_url("misc/city") }}',function(resp){
                var li = "";
                if(resp.rajaongkir.status.code === 200){
                    $.each(resp.rajaongkir.results,function(i,d){
                        li += "<option value='"+d.city_id+"'>"+d.city_name+" ("+d.type+")</option>";
                    })
                }
                $(elem.target).html(li);
            })
        }
    </script>
@stop