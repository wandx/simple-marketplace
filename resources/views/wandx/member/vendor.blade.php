@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Member</a></li>
        <li class="active">Vendor</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Vendor
        <small>List member with vendor role</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Merchant</th>
                    <th>Email</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <select id="search-by-status" class="form-control">
                            <option value="">All</option>
                            <option value="active">Active</option>
                            <option value="suspend">Suspend</option>
                        </select>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop


@section('prestyle')
    <link rel="stylesheet" href="/wandx/css/dataTables.bootstrap.min.css">
@stop

@section('scripts')
    <script src="/wandx/js/jquery.dataTables.min.js"></script>
    <script src="/wandx/js/dataTables.bootstrap.min.js"></script>
    <script>
        var table;
        $(function(){
            table = $('#dtable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('member.vendor.data') }}",
                "columns": [
                    {"data":"name","name":"name"},
                    {"data":"status","name":"status"},
                    {"data":"merchant","name":"merchant","orderable":false},
                    {"data":"email","name":"email"},
                    {"data":"actions","name":"actions","searchable":false,"orderable":false}
                ],
                "columnDefs":[
                    {
                        "targets":4,
                        "className":"text-center"
                    }
                ]
            });

            // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'select#search-by-status', this.footer() ).on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );


        });

        $(document).on('click','.suspend-btn',function(){
            var btn = $(this);
            suspend(btn.data('id'),btn.data('name'));
        });

        $(document).on('click','.restore-btn',function(){
            var btn = $(this);
            restore(btn.data('id'),btn.data('name'));
        });

        $(document).on('click','.destroy-btn',function(){
            var btn = $(this);
            destroy(btn.data('id'),btn.data('name'));
        });

        function suspend(id,name){
            var c = confirm('Suspend '+name+' ?');
            if(c){
                $.get('{{ backend_url('member/suspend') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }

        function restore(id,name){
            var c = confirm('Restore '+name+' ?');
            if(c){
                $.get('{{ backend_url('member/restore') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }

        function destroy(id,name){
            var c = confirm('Destroy '+name+' ?');
            if(c){
                $.get('{{ backend_url('member/destroy') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }
    </script>
@stop