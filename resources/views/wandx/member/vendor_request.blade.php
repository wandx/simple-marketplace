@extends('wandx.master')

@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="#">Member</a></li>
        <li class="active">User</li>
    </ol>
@stop

@section('page-header')
    <h1>
        Vendor Request
        <small>List member that request to be vendor</small>
    </h1>
@stop

@section('contents')
    <div class="panel">
        <div class="panel-body">
            <table class="table table-hover" id="dtable">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Merchant</th>
                    <th>Email</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>

                <tfoot>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <select id="search-by-status" class="form-control">
                            <option value="">All</option>
                            <option value="active">Active</option>
                            <option value="suspend">Suspend</option>
                        </select>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop

@section('modals')
    <!-- Modal -->
    <div id="detail-product" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detail</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@stop

@section('prestyle')
    <link rel="stylesheet" href="/wandx/css/dataTables.bootstrap.min.css">
@stop

@section('scripts')
    <script src="/wandx/js/jquery.dataTables.min.js"></script>
    <script src="/wandx/js/dataTables.bootstrap.min.js"></script>
    <script>
        var table;
        $(function(){
            table = $('#dtable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('member.vr.data') }}",
                "columns": [
                    {"data":"name","name":"name"},
                    {"data":"status","name":"status"},
                    {"data":"merchant","name":"merchant","orderable":false},
                    {"data":"email","name":"email"},
                    {"data":"actions","name":"actions","searchable":false,"orderable":false}
                ],
                "columnDefs":[
                    {
                        "targets":4,
                        "className":"text-center"
                    }
                ]
            });

            // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'select#search-by-status', this.footer() ).on( 'change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );


        });

        $(document).on('click','.accept-btn',function(){
            var btn = $(this);
            accept_request(btn.data('id'),btn.data('name'));
        });

        $(document).on('click','.reject-btn',function(){
            var btn = $(this);
            reject_request(btn.data('id'),btn.data('name'));
        });

        function accept_request(id,name){
            var c = confirm('Accept vendor request from '+name+' ?');
            if(c){
                $.get('{{ backend_url('member/vendor-request/accept') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }

        function reject_request(id,name){
            var c = confirm('Accept vendor request from '+name+' ?');
            if(c){
                $.get('{{ backend_url('member/vendor-request/reject') }}/'+id,function(data){
                    table.ajax.reload();
                });
            }
        }
    </script>
@stop