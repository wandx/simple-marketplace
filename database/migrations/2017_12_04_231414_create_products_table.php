<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedInteger('sub_category_id');
            $table->string('name');
            $table->double('price',11,2);
            $table->text('description');
            $table->float('rating',2,1)->default(0);
            $table->unsignedInteger('quantity')->default(0);
            $table->enum('status',['pending','active','suspend'])->default('pending');
            $table->uuid('user_id');
            $table->unsignedInteger('weight')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
