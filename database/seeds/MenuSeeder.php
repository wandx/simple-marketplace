<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::statement("SET FOREIGN_KEY_CHECKS = 0");
        DB::table('menus')->truncate();
        DB::table('sub_menus')->truncate();
        DB::table('sub_sub_menus')->truncate();

        DB::table('menus')->insert([
            [
                'name' => 'Dashboard',
                'slug' => 'dashboard',
                'icon' => 'fa fa-dashboard'
            ],
            [
                'name' => 'User Manager',
                'slug' => 'usermanager',
                'icon' => 'fa fa-users'
            ],
            [
                'name' => 'Member',
                'slug' => 'member',
                'icon' => 'fa fa-user'
            ],
            [
                'name' => 'Product',
                'slug' => 'product',
                'icon' => 'fa fa-shopping-cart'
            ],
            [
                'name' => 'Point of Sale',
                'slug' => 'pos',
                'icon' => 'fa fa-cart-plus'
            ]
        ]);

        DB::table('sub_menus')->insert([
            [
                'name' => 'Lists',
                'slug' => 'lists',
                'icon' => 'fa fa-tasks',
                'menu_id' => 2
            ],
            [
                'name' => 'Roles',
                'slug' => 'roles',
                'icon' => 'fa fa-tasks',
                'menu_id' => 2
            ],
            [
                'name' => 'User',
                'slug' => 'user',
                'icon' => 'fa fa-circle',
                'menu_id' => 3
            ],
            [
                'name' => 'Vendor',
                'slug' => 'vendor',
                'icon' => 'fa fa-circle',
                'menu_id' => 3
            ],
            [
                'name' => 'Vendor Request',
                'slug' => 'vendor-request',
                'icon' => 'fa fa-circle',
                'menu_id' => 3
            ]
        ]);
        //DB::statement("SET FOREIGN_KEY_CHECKS = 1");
    }
}
