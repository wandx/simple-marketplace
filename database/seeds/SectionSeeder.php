<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sections')->truncate();
        DB::table('sections')->insert([
            [
                'name' => 'setting.global_commission',
                'value' => '0'
            ],
            [
                'name' => 'setting.per_product_commission',
                'value' => '10%'
            ]
        ]);
    }
}
