<?php

use Illuminate\Database\Seeder;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->truncate();
        $f = \Faker\Factory::create();

        for($i=0;$i<10;$i++){
            (new \App\Models\Bank())->create([
                'name' => $f->words(2,true),
                'code' => rand(11,99),
                'show' => 1
            ]);
        }
    }
}
