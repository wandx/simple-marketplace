<?php

use Illuminate\Database\Seeder;

class AllCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();
        DB::table('sub_categories')->truncate();
        $f = \Faker\Factory::create('id_ID');
        for($i=1;$i<10;$i++){
            $cat = (new \App\Models\Category())->create([
                'name' => $name = $f->words(1,true),
                'slug' => $name,
                'description' => $f->paragraphs(2,true)
            ]);

            for($i2=0;$i2<5;$i2++){
                $cat->sub_categories()->create([
                    'name' => $name = $f->words(1,true),
                    'slug' => $name,
                    'description' => $f->paragraphs(2,true)
                ]);
            }
        }

    }
}
