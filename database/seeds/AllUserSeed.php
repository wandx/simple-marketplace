<?php

use Illuminate\Database\Seeder;

class AllUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // faker
        $f = \Faker\Factory::create('id_ID');
        DB::table('users')->truncate(); // user
        DB::table('addresses')->truncate(); // address
        DB::table('merchants')->truncate(); // merchant
        DB::table('products')->truncate(); // product
        DB::table('product_images')->truncate(); // product image

        for($i=0;$i<50;$i++){
            $is_vendor = rand(1,10)%2 == 0 ? 1:0;
            $user = (new \App\Models\User())->create([
                'id' => $f->uuid,
                'name' => $name = $f->name,
                'email' => $f->email,
                'password' => bcrypt('password'),
                'is_vendor' => $is_vendor,
            ]);

            // address
            for($i2=0;$i2<2;$i2++){
                $user->addresses()->create([
                    'id' => $f->uuid,
                    'name' => $f->words(3,true),
                    'address' => $f->address,
                    'district_id' => rand(1,50),
                    'hint' => $f->paragraphs(2,true)
                ]);
            }

            // merchant
            if($is_vendor == 1){
                $user->merchant()->create([
                    'id' => $f->uuid,
                    'name' => $f->words(3,true),
                    'description' => $f->paragraphs(3,true),
                    'acc_name' => $name,
                    'acc_number' => rand(111111111,999999999),
                    'bank_id' => rand(1,10),
                    'status' => 'active'
                ]);

                for($i3=1;$i3<4;$i3++){
                    $product = $user->products()->create([
                        'sub_category_id' => rand(1,50),
                        'name' => $f->words(3,true),
                        'price' => rand(10000,3000000),
                        'description' => $f->paragraphs(4,true),
                        'rating' => rand(1,5),
                        'quantity' => rand(1,100),
                        'status' => 'active'
                    ]);

                    for($i4=0;$i4<3;$i4++){
                        $product->images()->create([
                            'id' => $f->uuid,
                            'thumb' => 'http://placehold.it/200x300',
                            'original' => 'http://placehold.it/600x900',
                            'square' => 'http://placehold.it/100x100'
                        ]);
                    }
                }
            }
        }

    }
}
